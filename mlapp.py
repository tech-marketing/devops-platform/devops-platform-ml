import spacy
import numpy as np
import pandas as pd
import streamlit as st
from whatlies.transformers import Pca
from whatlies.language import SpacyLanguage
from whatlies.language import BytePairLanguage

nlp_md = spacy.load("en_core_web_md")



bp_lang = BytePairLanguage("en")


def embeddings(corpus):
    return bp_lang[corpus]

st.title("Gitlab - Streamlit ML Application")
st.write("Deployed using Gitlab CI/CD  and Kubernetes")

words = ['university', 'college', 'school', 'army', 'tank', 'aircraft',
         'cat', 'dog', 'lion', 'puppy', 'student', 'teacher',
         'lesson', 'mobile', 'kitten']

corpus = ["prince", "princess", "nurse", "doctor", "banker", "man", "woman",
         "cousin", "neice", "king", "queen", "dude", "guy", "gal", "fire",
         "dog", "cat", "mouse", "red", "blue", "green", "yellow", "water",
         "person", "family", "brother", "sister"]

df = pd.DataFrame(corpus, columns=['words'])
st.dataframe(df)

#embedding_set =bp_lang[words]


embedding_set = embeddings(words)

colors = ["red", "blue", "green", "yellow"]

lang = SpacyLanguage("en_core_web_md")
embedding_set = (lang[corpus]
            .transform(Pca(2))
            .assign(is_color=lambda e: e.name in colors))

st.write(embedding_set.plot_brush(n_show=15, color="is_color"))


st.title("Add your own words")
user_input = st.text_input("Let's calculate word embeddings", "cat dog")

st.write(user_input)
print(user_input)
print(type(user_input))

words_input = user_input.split()

embedding_set_2 =bp_lang[words_input]

embedding_set_2 = (lang[words_input]
            .transform(Pca(2))
            .assign(is_color=lambda e: e.name in colors))

st.write(embedding_set_2.plot_brush(n_show=15, color="is_color"))



x = st.slider("Slope", min_value = 0.01, max_value = 0.1, step=0.01)
y = st.slider("Noise", min_value=0.01, max_value=0.1, step=0.01)

st.write(f"x={x} y={y}")

